const renderTheme = (colors) => `
colors = {
    'shade': [
        '${colors.shade0}',
        '${colors.shade1}',
        '${colors.shade2}',
        '${colors.shade3}',
        '${colors.shade4}',
        '${colors.shade5}',
        '${colors.shade6}',
        '${colors.shade7}'
    ],
    'accent': [
        '${colors.accent0}',
        '${colors.accent1}',
        '${colors.accent2}',
        '${colors.accent3}',
        '${colors.accent4}',
        '${colors.accent5}',
        '${colors.accent6}',
        '${colors.accent7}'
    ]
}
`;

const render = (colors) =>
  Object.entries(colors).map(async ([name, colors]) => ({
    name: `themer-${name}.py`,
    contents: Buffer.from(renderTheme(colors), 'utf8'),
  }));

module.exports = { render };
