# Themer for Qtile

This is a theme plugin for [Themer](https://github.com/themerdev/themer), that produces opinionated theme files for use with the Qtile Window Manager for Linux.

## Ouput File Format

```python
colors = {
    'shade': [
        'shade0',
        'shade1',
        'shade2',
        'shade3',
        'shade4',
        'shade5',
        'shade6',
        'shade7'
    ],
    'accent': [
        'accent0',
        'accent1',
        'accent2',
        'accent3',
        'accent4',
        'accent5',
        'accent6',
        'accent7'
    ]
}
```
